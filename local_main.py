from PIL import Image
import datetime as dt
from io import BytesIO
from gather_radar_images import (download_latest_image, radars)
from default_logger import  global_logger


def main():
    image_data = download_latest_image(utcnow=dt.datetime.utcnow(), radar_prefix=radars['Sydney128k'], logger=global_logger)
    img = Image.open(BytesIO(image_data['data']))
    img.show()

if __name__ == '__main__':
    main()