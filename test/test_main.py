from main import save_latest_radar_images
import unittest
from unittest import mock
import base64


class TestMain(unittest.TestCase):

    # def setUp(self):
    #     self.app = hello_world.app.test_client()
    #     self.app.testing = True

    @mock.patch("google.cloud.storage.Client")
    def test_save_latest_radar_images(self, mock_client):
        expectedMsg = 'Hello from PubSub!'
        event = {}
        event['data'] = base64.b64encode(expectedMsg.encode('utf-8'))
        result = save_latest_radar_images(event, None)
        self.assertTrue(expectedMsg in result)
        self.assertFalse('Some other message' in result)


if __name__ == '__main__':
    unittest.main()