import unittest
from unittest import mock
from store_radar_images import store_radar_image_map
from test.test_utils import TestLogger


class TestStoreRadarImages(unittest.TestCase):

    def setUp(self):
        self.logger = TestLogger()

    @mock.patch("google.cloud.storage.Client")
    def test_upload_file(self, mock_client):
        # run function just recording interaction through mock
        radar_image_map = {'Sydney128k': {'filename': 'IDR713.T.202001160248.png', 'data': 'IMAGE_DATA', 'type': 'image/png'}}
        store_radar_image_map(radar_image_map, self.logger)

        # assert bucket was called with the passed string
        bucket = mock_client().get_bucket
        bucket.assert_called_with("bucket")

        # assert blob and upload were called with expected params
        blob = bucket().blob
        blob.assert_called_with("data/images/original/Sydney128k/IDR713.T.202001160248.png")
        blob().upload_from_string.assert_called_with(data="IMAGE_DATA", content_type='image/png')
