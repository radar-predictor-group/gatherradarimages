from gather_radar_images import (radars, generate_filename, download_image, download_latest_image,
                                 download_all_latest_images)
import urllib.request
from test.test_utils import TestLogger
import unittest
from unittest.mock import patch, MagicMock
import datetime as dt


class TestGatherRadarImages(unittest.TestCase):

    def setUp(self):
        self.logger = TestLogger()

    def test_generate_filename(self):
        date_and_time = dt.datetime(year=2020, month=1, day=20, hour=13, minute=12)
        self.assertEqual(generate_filename('IDR713', date_and_time), 'IDR713.T.202001201312.png')


    @patch('urllib.request.urlopen')
    def test_no_cm(self, mock_urlopen):
        cm = MagicMock()
        cm.getcode.return_value = 404
        cm.read.return_value = 'contents'
        mock_urlopen.return_value = cm

        response = urllib.request.urlopen('http://foo')
        self.assertEqual(response.getcode(), 404)
        response.close()


    @patch('urllib.request.urlopen')
    def test_mock_successful_download_image(self, mock_urlopen):
        cm = MagicMock()
        cm.getcode.return_value = 200
        cm.read.return_value = 'contents'
        cm.__enter__.return_value = cm
        mock_urlopen.return_value = cm

        self.assertTrue(download_image('', self.logger))


    @patch('urllib.request.urlopen')
    def test_mock_failed_download_image(self, mock_urlopen):
        cm = MagicMock()
        cm.getcode.return_value = 401
        cm.read.return_value = None
        cm.__enter__.return_value = cm
        mock_urlopen.return_value = cm
        self.assertFalse(download_image('', self.logger))


    def test_download_latest_image(self):
        utcTimeNow = dt.datetime.utcnow()
        # download_latest_image should always find something to download as it traverses
        # backwards from the current time to find the most recent image to download.
        # This could fail if the internet connection is bad, the BoM goes down, or it
        # finds a way to block this type of scraping.
        download_result = download_latest_image(utcnow=utcTimeNow, radar_prefix=radars['Sydney128k'], logger=self.logger)
        self.assertTrue(download_result['data'])
        self.assertEqual(download_result['type'], 'image/png')


    def test_download_all_latest_images(self):
        utcTimeNow = dt.datetime.utcnow()
        latest_radar_images = download_all_latest_images(utcTimeNow, self.logger)
        for radar_name in latest_radar_images:
            self.assertTrue(latest_radar_images[radar_name], 'No image downloaded for radar prefix "%s"' % radar_name)


if __name__ == '__main__':
    unittest.main()