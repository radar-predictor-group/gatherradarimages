import urllib.request
import datetime as dt
import time
from requests import HTTPError as ReqHttpError
from urllib.error import HTTPError as UrlLibHttpError

radars = {'Sydney512k': 'IDR711',
          'Sydney256k': 'IDR712',
          'Sydney128k': 'IDR713',
          'Sydney64k': 'IDR714',
          'Darwin512k': 'IDR631',
          'Darwin256k': 'IDR632',
          'Darwin128k': 'IDR633',
          'Darwin64k': 'IDR634'}

radar_names = radars.keys()

RADAR_IMAGE_UPDATE_FREQUENCY_MINS = 6
NUM_RETRIES = 3


def generate_filename(radar_prefix, date_and_time):
    date_time_string = date_and_time.strftime("%Y%m%d%H%M")
    return '%s.T.%s.png' % (radar_prefix, date_time_string)


def read_url_as_user(url, logger):
    hdrs = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'}

    req = urllib.request.Request(url, headers=hdrs)
    for _ in range(NUM_RETRIES):
        try:
            with urllib.request.urlopen(req) as data_response:
                data = data_response.read()
                if data:
                    return {'data': data, 'type': data_response.info().get_content_type()}
                else:
                    return None
            time.sleep(0.3)
        except ReqHttpError as reqHttpError:
            logger.log("ReqHttpError: {}".format(reqHttpError))
        except UrlLibHttpError as urlLibHttpError:
            logger.log("UrlLibHttpError: {}".format(urlLibHttpError))
            pass
    return None


def download_image(remote_image_name, logger):
    image_url = 'http://www.bom.gov.au/radar/%s' % remote_image_name
    return read_url_as_user(image_url, logger)


def download_latest_image(utcnow, radar_prefix, logger):
    for minutes in range(2 * RADAR_IMAGE_UPDATE_FREQUENCY_MINS + 1):
        date_time_to_check = utcnow - dt.timedelta(minutes=minutes)
        remote_image_name = generate_filename(radar_prefix=radar_prefix, date_and_time=date_time_to_check)
        image_data = download_image(remote_image_name, logger)
        if image_data:
            logger.log('Succeeded in downloading image "%s"' % remote_image_name)
            return {'filename': remote_image_name, 'data': image_data['data'], 'type': image_data['type']}
    return None


def download_all_latest_images(utcnow, logger):
    # for each radar, we need to search back up to 6 minutes from the current time
    # till we find an image to download. We do this as we can't assume we are synced
    # up with the 6 minute cycle of the BoM for generating the images
    latest_radar_images = {}
    for radar in radar_names:
        radar_prefix = radars[radar]
        latest_radar_images[radar] = download_latest_image(utcnow, radar_prefix, logger)
    return latest_radar_images
