import base64
import datetime as dt
from gather_radar_images import download_all_latest_images
from store_radar_images import store_radar_image_map
from default_logger import  global_logger


def save_latest_radar_images(event, context):
    """Triggered from a message on a Cloud Pub/Sub topic.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    utcTimeNow = dt.datetime.utcnow()
    radar_image_map = download_all_latest_images(utcTimeNow, global_logger)
    radar_image_map_printable = dict(zip(radar_image_map.keys(), ['PNG-Found' if x else 'No-PNG' for x in radar_image_map.values()]))
    global_logger.log("Downloaded the following radar data: %s" % radar_image_map_printable)

    store_radar_image_map(radar_image_map, global_logger)

    pubsub_message = base64.b64decode(event['data']).decode('utf-8')

    execution_time = dt.datetime.utcnow() - utcTimeNow

    logMsg = "CI/CD deployed function at '{}' received message: {}. Execution Time: {}".format(dt.datetime.utcnow(), pubsub_message, execution_time)
    global_logger.log(logMsg)
    return logMsg
