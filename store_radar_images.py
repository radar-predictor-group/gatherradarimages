import os
from google.cloud import storage

BASE_PATH = 'data/images/original/'
BUCKET_NAME = os.environ['GCP_BUCKET_NAME']


def upload_file(filename, content, content_type, bucket, logger):
    blob_filename = BASE_PATH + filename
    blob = bucket.blob(blob_filename)
    blob.upload_from_string(data=content, content_type=content_type)
    logger.log('Uploaded data to path [{}]'.format(blob_filename))


def store_radar_image_map(radar_image_map, logger):
    logger.log('Uploading to bucket named [{}]'.format(BUCKET_NAME))
    bucket = storage.Client().get_bucket(BUCKET_NAME)
    for radar_name in radar_image_map:
        image_filename = '{}/{}'.format(radar_name, radar_image_map[radar_name]['filename'])
        upload_file(image_filename, radar_image_map[radar_name]['data'], radar_image_map[radar_name]['type'], bucket, logger)
